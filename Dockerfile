FROM r-base:4.2.2

RUN apt-get -y update && apt-get -y install libssl-dev libpq-dev pandoc \
    texlive-latex-base texlive-latex-extra texlive-latex-recommended libxml2-dev libudunits2-dev

RUN apt-get -y update && apt-get -y install r-cran-ggplot2 r-cran-rserve r-cran-dbi r-cran-rpostgresql \
    r-cran-gridgraphics binutils

RUN apt-get -y update && apt-get -y install r-cran-parallelmap r-cran-r6 r-cran-bh r-cran-xml2 r-cran-stringi \
    r-cran-readr r-cran-rcpparmadillo r-cran-dplyr r-cran-emmeans r-cran-lme4 r-cran-lubridate \
    r-cran-mass r-cran-rlang r-cran-robustbase r-cran-digest r-cran-dt r-cran-htmltools r-cran-knitr \
    r-cran-rmarkdown r-cran-rstudioapi r-cran-testthat r-cran-tibble r-cran-matrixstats r-cran-forcats \
    r-cran-progress r-cran-zip r-cran-sparsem r-cran-matrixmodels r-cran-sp r-cran-haven r-cran-curl \
    r-cran-readxl r-cran-openxlsx r-cran-cardata r-cran-abind r-cran-pbkrtest r-cran-quantreg r-cran-maptools \
    r-cran-rio r-cran-broom r-cran-car r-cran-ggsci r-cran-tidyr r-cran-gridextra r-cran-reshape r-cran-triebeard \
    r-cran-urltools r-cran-sys r-cran-askpass r-cran-openssl r-cran-httr r-cran-selectr r-cran-fs r-cran-magick r-cran-rvest \
    r-cran-reshape2 r-cran-pander r-cran-xfun r-cran-cachem r-cran-sass r-cran-shiny r-cran-dt r-cran-rappdirs r-cran-tinytex

COPY Rprofile.site /etc/R/

RUN echo "TZ=Etc/UTC\nR_HOME=/usr/lib/R" >> /etc/R/Renviron

RUN R -e 'install.packages("rvcheck"); library(rvcheck)'
RUN R -e 'install.packages("readODS"); library(readODS)'
RUN R -e 'install.packages("R.devices"); library(R.devices)'
RUN R -e 'install.packages("qmrparser"); library(qmrparser)'
RUN R -e 'install.packages("dbx"); library(dbx)'
RUN R -e 'install.packages("tryCatchLog"); library(tryCatchLog)'
RUN R -e 'install.packages("tth"); library(tth)'
RUN R -e 'install.packages("unix"); library(unix)'
RUN R -e 'install.packages("ggplotify"); library(ggplotify)'
RUN R -e 'install.packages("latexpdf"); library(latexpdf)'
RUN R -e 'install.packages("splitstackshape"); library(splitstackshape)'
RUN R -e 'install.packages("MultinomialCI"); library(MultinomialCI)'
RUN R -e 'install.packages("bslib"); library(bslib)'
RUN R -e 'install.packages("rmarkdown"); library(rmarkdown)'
RUN R -e 'install.packages("flexdashboard"); library(flexdashboard)'
RUN R -e 'install.packages("xfun"); library(xfun)'
RUN R -e 'install.packages("cachem"); library(cachem)'
RUN R -e 'install.packages("ggvenn"); library(ggvenn)'
RUN R -e 'install.packages("sass"); library(sass)'
RUN R -e 'install.packages("jquerylib"); library(jquerylib)'
RUN R -e 'install.packages("shiny"); library(shiny)' # for static site generation, DT uses parts of shiny if available
RUN R -e 'install.packages("DT"); library(DT)' # DT must be installed in a version >= 0.18: Strange bug only on Debian -- also stated in the deps of squarereportrenderer; r-cran-dt in bullseye is 0.17+dfsg-3
RUN R -e 'install.packages("rappdirs"); library(rappdirs)'
RUN R -e 'install.packages("tinytex"); library(tinytex)'

RUN R -e 'install.packages("Rserve",,"http://rforge.net/",type="source");' # update to a more recent version not hosted at cran

RUN echo '#!/usr/bin/bash' > /usr/local/bin/rserve && chmod +x /usr/local/bin/rserve
RUN echo 'R_HOME=/usr/lib/R /usr/lib/R/site-library/Rserve/libs/Rserve.dbg --no-save' > /usr/local/bin/rserve && chmod +x /usr/local/bin/rserve


ADD "https://packages.qihs.uni-greifswald.de//service/rest/v1/search?repository=ship-snapshot-r&name=pandocconverter&sort=version" /tmp/pandocconverter
RUN R -e 'install.packages("pandocconverter"); library(pandocconverter)'

ADD "https://packages.qihs.uni-greifswald.de//service/rest/v1/search?repository=ship-snapshot-r&name=dataquieR&sort=version" /tmp/dataquieR
RUN R -e 'install.packages("dataquieR"); library(dataquieR)'

ADD "https://packages.qihs.uni-greifswald.de//service/rest/v1/search?repository=ship-snapshot-r&name=functionregistrator&sort=version" /tmp/functionregistrator
RUN R -e 'install.packages("functionregistrator"); library(functionregistrator)'

ADD "https://packages.qihs.uni-greifswald.de//service/rest/v1/search?repository=ship-snapshot-r&name=squaredbconnector&sort=version" /tmp/squaredbconnector
RUN R -e 'install.packages("squaredbconnector"); library(squaredbconnector)'

ADD "https://packages.qihs.uni-greifswald.de//service/rest/v1/search?repository=ship-snapshot-r&name=squarerepositoryspec&sort=version" /tmp/squarerepositoryspec
RUN R -e 'install.packages("squarerepositoryspec"); library(squarerepositoryspec)'

ADD "https://packages.qihs.uni-greifswald.de//service/rest/v1/search?repository=ship-snapshot-r&name=sq2psqlRepository&sort=version" /tmp/sq2psqlRepository
RUN R -e 'install.packages("sq2psqlRepository"); library(sq2psqlRepository)'

ADD "https://packages.qihs.uni-greifswald.de//service/rest/v1/search?repository=ship-snapshot-r&name=SquareControl2&sort=version" /tmp/SquareControl2
RUN R -e 'install.packages("SquareControl2"); library(SquareControl2)'

ADD "https://packages.qihs.uni-greifswald.de//service/rest/v1/search?repository=ship-snapshot-r&name=SquareControlImpl&sort=version" /tmp/SquareControlImpl
RUN R -e 'install.packages("SquareControlImpl"); library(SquareControlImpl)'

ADD "https://packages.qihs.uni-greifswald.de//service/rest/v1/search?repository=ship-snapshot-r&name=squaremetadatainterface&sort=version" /tmp/squaremetadatainterface
RUN R -e 'install.packages("squaremetadatainterface"); library(squaremetadatainterface)'

ADD "https://packages.qihs.uni-greifswald.de//service/rest/v1/search?repository=ship-snapshot-r&name=squarereportrenderer&sort=version" /tmp/squarereportrenderer
RUN R -e 'install.packages("squarereportrenderer"); library(squarereportrenderer)'

ADD "https://packages.qihs.uni-greifswald.de//service/rest/v1/search?repository=ship-snapshot-r&name=opalRepository&sort=version" /tmp/opalRepository
RUN R -e 'install.packages("opalRepository"); library(opalRepository)'

RUN apt-get -y update && apt-get -y install libcurl4-openssl-dev

ADD "https://packages.qihs.uni-greifswald.de//service/rest/v1/search?repository=ship-snapshot-r&name=sq2filerepository&sort=version" /tmp/sq2filerepository
RUN R -e 'install.packages("sq2filerepository"); library(sq2filerepository)'

COPY Rserv.conf /etc/Rserv.conf
COPY squaredbconnector.properties /etc/squaredbconnector.properties

RUN mkdir /userLibrary
RUN echo '.libPaths( c( "/userLibrary" , .libPaths() ) )' >> /etc/R/Rprofile.site

RUN mkdir -p /etc/square_foreign
RUN mkdir -p /square_results

#
# Finally launch runit.
#
ENTRYPOINT ["/bin/bash", "/usr/local/bin/rserve"]

EXPOSE 6311
